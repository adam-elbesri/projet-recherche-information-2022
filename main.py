from io import TextIOWrapper
import os
from indexer import Indexer
import argparse
import time
import operator
import pandas as pd
import matplotlib.pyplot as plt
from nltk.stem import PorterStemmer
import math

FILENAME = "File"
NB_TOKENS = "Nb of tokens"
TIME = "Time (s)"
ps = PorterStemmer()


def parseArguments():
    parser = argparse.ArgumentParser(description="Indexes documents.")
    parser.add_argument('files', metavar='<file>', nargs='+', type=str, help="A file to be indexed (can be gzipped)")
    parser.add_argument('runid', metavar='<runID>', type=int, help="Arbitrary ID of the run, to be printed in results")
    parser.add_argument('--print-index', dest='silent', action='store_false', help="Prints the index")
    parser.add_argument('--no-progress', dest='progress', action='store_false', help="Do not print progress bars")
    parser.add_argument('--time', dest='printTimeTable', action='store_true', help="Prints a time table")
    parser.add_argument('-o', metavar="<outputFile>", dest='outputPath', nargs='?', type=str,
                        help="Name of output graph")
    parser.add_argument('--show-graph', dest='showGraph', action='store_true',
                        help="Show the graph in a separate windows after indexing")
    parser.add_argument('-s', '--save-index', metavar="<indexFile>", dest='indexFile', nargs='?', type=str,
                        const="index.bin", help="Saves the index into a binary file. (Default \"index.bin\")")
    parser.add_argument('-f', '--force', dest='forceSave', action='store_true',
                        help="If -s specified but file already exists, overwrite anyway")
    parser.add_argument('-l', '--load-index', metavar="<indexFile>", dest='loadFile', nargs='?', const="index.bin", type=str,
                        help="Load indexer from file (default \"index.bin\")")
    parser.add_argument('--sltn', dest='Smartltn', action='store_true', help=" Lancement du Smart LTN run")
    parser.add_argument('--bm25', dest='BM25', action='store_true', help=" Lancement de BM25 run")
    parser.add_argument('--stopwords', dest='StopWords', action='store_true', help="Indexation avec les stop words")
    parser.add_argument('--stemmer', dest='Stemmer', action='store_true', help="Indexation avec Porter Stemmer")
    parser.add_argument('--elements', dest='Element', action='store_true', help="Parametrage de la runs en mode elements, par default Les runs sont articles")
    parser.add_argument('-granularite', metavar="<granularite>", dest='granularite', type=int,
                        help="Argument qui ajuste la granularite pour les chemins xml, Par default une granularite de 5")
    parser.add_argument('-k', metavar="<k>", dest='k', type=float,
                        help="Constante K pour bm25 par defaut 1")
    parser.add_argument('-b', metavar="<b>", dest='b', type=float,
                        help="Constante b pour bm25 par defaut 0.5")



    return parser.parse_args()


def printTimeTable(timeTable: dict) -> None:
    """Prints the time taken for indexing each file"""
    df = pd.DataFrame(timeTable)
    print(df)


def generateGraph(timeTable: dict, outputPath: str, showGraph: bool = False) -> None:
    """Saves a graph of the indexation time"""
    x = timeTable[NB_TOKENS]
    y = timeTable[TIME]
    plt.plot(x, y, 'bo')
    plt.xlabel("Number of tokens (log)")
    plt.xscale('log')
    plt.ylabel('Time (s)')
    plt.title("Time taken to index tokens in documents")
    plt.grid(True)
    outputPath = os.path.basename(outputPath)
    outputPath, _ = os.path.splitext(outputPath)
    plt.savefig(str(outputPath))
    if showGraph:
        plt.show()
    print("Saved time graph to " + str(outputPath) + ".png")


def plotStat(dictionnaire: dict, nomy, nomgraph) -> None:
    plt.plot(*zip(*sorted(dictionnaire.items())))
    plt.xlabel("Number of tokens (log)")
    plt.xscale('log')
    plt.ylabel(nomy)
    plt.title(nomgraph)
    plt.show()


def processFile(indexer : Indexer, filename, timeTable, progress) -> int :
    """Makes the indexer index given file, and measuring time taken at the same time"""
    name, _ = os.path.splitext(filename)

    file = filename

    print(f"Processing file {filename}")

    start = time.time()
    nbTokens = indexer.index(file, progress=progress)
    end = time.time()

    timeTable[FILENAME] += [os.path.basename(name)]
    timeTable[NB_TOKENS] += [nbTokens]
    timeTable[TIME] += [end - start]

    return nbTokens


def main():
    args = parseArguments()
    indexer = Indexer()
    timeTable = {FILENAME: [], NB_TOKENS: [], TIME: []}
    tokenmax = 0
    elements = False
    granularite = 5

    if (args.granularite):
        granularite = args.granularite
    if (args.Element):
        elements = True

    load = args.loadFile
    if(args.StopWords):
        indexer.stopWords = True

    if (args.Stemmer):
        indexer.porterStemmer = True

    if load: # load the index from file
        indexer = indexer.loadIndexAs(load)
        print("[INFO] Index file " + str(load) + " successfully loaded")
    else: # index given file(s)
        for filepath in args.files:
            if not os.path.isdir(filepath):
                tokenmax += processFile(indexer, filepath, timeTable, args.progress)
            else:
                for dirpath, _, filenames in os.walk(filepath):
                    for filename in filenames:
                        tokenmax += processFile(indexer, os.path.join(dirpath, filename), timeTable, args.progress)

    indexFile = args.indexFile
    if indexFile:
        if not os.path.exists(indexFile) or args.forceSave:
            isSaved = indexer.saveIndexAs(indexFile)
            if isSaved:
                print("[INFO] Index file successfully saved")
            else:
                print("[ERROR] Index file could not be saved")
        else:
            print(
                f"[ERROR] File already exists: \"{indexFile}\". Either save it with a different name or specify option -f (--force) to overwrite")

    if not args.silent:
        indexer.printIndex()

    if args.printTimeTable:
        printTimeTable(timeTable)

    if args.outputPath != None:
        generateGraph(timeTable, args.outputPath, args.showGraph)

    ## -----------------------------------Code pour le scoring ----------------------------------------------------------------------
   
    Sltn = args.Smartltn
    bm25 = args.BM25

    querydict = {}

    queryfilename = "queries.txt"
    queries = list(map(lambda line: (line.split()[0], " ".join(line.split()[1:])), open(queryfilename, "r").readlines()))
    for query in queries :
        querydict[query[0]] = query[1]

    if not Sltn and not bm25:
        print("Rien a faire de plus")
        exit(0)

    if not os.path.exists("Resultat"):
        os.makedirs("Resultat")


    resultsFile = createEmptyResultsFile(elements,args.runid, getScoringFunction(args), args.Stemmer, args.StopWords)

    # Pour chaque query
    for keys in querydict:

        query = querydict[keys]
        queryid = keys
        list_of_docs_query = []
        for word in query.split():
            try:
                # Si stemmer, on stem la query
                if (args.Stemmer):
                    list_of_docs_query += indexer.postingsLists[ps.stem(word)]
                else:
                    list_of_docs_query += indexer.postingsLists[word]
            except KeyError:
                continue

        # -----------------Smart LTN---------------

        if Sltn:

            ltn_weights = {}
            for doc in list_of_docs_query:
                ltn_weights[doc] = {}
                final_score = 0

                for word in query.split():
                    try:
                        if doc in indexer.postingsLists[word]:
                            weight = (1 + math.log10(indexer.termFrequencies[word][doc])) * math.log10(
                                len(indexer.documentsLength) / len(indexer.postingsLists[word]))
                        else:
                            weight = 0
                    except KeyError:
                        continue

                    final_score += weight

                ltn_weights[doc] = final_score

            step = 1
            # on écrit le résultat du scoring de la query dans le run
            for k, v in sorted(ltn_weights.items(), reverse=True, key=operator.itemgetter(1))[:1500]:
                xml = guettoxml(indexer,k,query,elements,granularite)

                line = queryid + " Q0 " + str(k) + " " + str(step) + " " + str(v) + " SLAR/ltn "+xml+" \n"
                resultsFile.write(line)
                step += 1
            print("[INFO] Smart ltn has been evaluated")

        # --------------- BM25 ------------------

        if bm25:
            # default values
            k = 1.0
            b = 0.5
            if(args.k):
                k = args.k
            if (args.b):
                b = args.b

            sum_of_values = sum(indexer.dl.values())
            num_of_values = len(indexer.dl)
            avg = sum_of_values / num_of_values

            N = len(indexer.documentsLength) # number of documents

            ltn_weights = {}

            for doc in list_of_docs_query:
                ltn_weights[doc] = {}
                final_score = 0

                for word in query.split():

                    try:
                        df_word = len(indexer.postingsLists[word])
                        if doc in indexer.postingsLists[word]:
                            tf_word = indexer.termFrequencies[word][doc]
                        else:
                            tf_word = 0
                    except KeyError:
                        continue

                    dl = indexer.documentsLength[doc]

                    tf_part = tf_word * (k + 1) / (k * ((1 - b) + b * dl / avg + tf_word))
                    idf_part = math.log10((N - df_word + 0.5) / (df_word + 0.5))

                    weight = tf_part * idf_part
                    final_score += weight

                ltn_weights[doc] = final_score

            step = 1
            for id, score in sorted(ltn_weights.items(), reverse=True, key=operator.itemgetter(1))[:1500]:

                xml = guettoxml(indexer, id, query,elements,granularite)

                line = queryid + " Q0 " + str(id) + " " + str(step) + " " + str(score) + " SLAR/BM25 "+xml+" \n"
                resultsFile.write(line)
                step = step + 1
            print("[INFO] BM25 has been evaluated")

    resultsFile.close()

    return 0


def createEmptyResultsFile(elements,runid: int, scoring: str, hasStemmer: bool = False, hasStopWords: bool = False, nbStopWords: int = 319) -> TextIOWrapper:
    """Create empty file with correct filename"""
    stopwords = f"_stop{nbStopWords}" if hasStopWords else "_nostop"
    stemmer = "_porter" if hasStemmer else "_nostem"
    params = "_k1_b0.5" if scoring == "bm25" else ""
    typerun ="articles"
    if (elements):
        typerun = "elements"
    filename = f"SouhailAdamLoicRoman_{runid}_practice5_{scoring}_{typerun}{stopwords}{stemmer}{params}.txt"
    
    return open(os.path.join("Resultat", filename), "w")


def getScoringFunction(args) -> str :
    """Return corresponding constant"""
    if args.BM25:
        return "bm25"
    elif args.Smartltn:
        return "ltn"
    else:
        return "?"


def guettoxml(indexer, id ,query,elements,granularite) -> str:
    """Return the XML path of the element, following the query and granularity"""
    xml = "/article[1]"
    if(not elements)  :
        return xml
    for word in query.split():
        chemin = indexer.getXmlPathALTER(word, id)
        if chemin != []:
            temp = next(iter(chemin))
            xml = chemin[temp][0]
            xml = '/'.join(xml.split('/')[:granularite])
            xml = "/"+xml
            break
    return xml


if __name__ == "__main__":
    exit(main())