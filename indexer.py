import re
from operator import itemgetter
from sys import stderr
import xml.etree.ElementTree as ET
from tqdm import tqdm
from nltk.stem import PorterStemmer
import pickle
from os import path

WORD = 0
DOCID = 1


ps = PorterStemmer()
stopwords = open("stopwords.txt", 'r').read().splitlines()


class Indexer:
    """Class to index tokens from files"""
    def __init__(self) -> None:
        self._pl = {}
        """postings lists"""
        self._df = {}
        """document frequency of each term"""
        self._tf = {}
        """term frequency of each term in each document e.g.: tf["the"] = {1 => 2, 2 => 3, 3 => 1}"""
        self._dl = dict()
        """ Document length"""
        self._xp = {}
        """ Token XML Path"""
        self._stopwords = False
        """ Use stop words """
        self._stemmer = False
        """ Use Porter Stemmer"""
        self._tags = dict()
        """ List of tags for each document"""





    @property
    def postingsLists(self):
        return self._pl

    @property
    def xmlTags(self):
        return self._tags
    @property
    def stopWords(self):
        return self._stopwords

    @property
    def porterStemmer(self):
        return self._stemmer

    @property
    def xmlPaths(self):
        return self._xp

    @property
    def documentFrequencies(self):
        return self._df

    @property
    def termFrequencies(self):
        return self._tf

    @property
    def documentsLength(self):
        return self._dl

    @postingsLists.getter
    def postingsLists(self):
        return self._pl

    @porterStemmer.getter
    def porterStemmer(self):
        return self._stemmer

    @stopWords.getter
    def stopWords(self):
        return self._stopwords

    @xmlPaths.getter
    def xmlPaths(self):
        return self._xp

    @xmlTags.getter
    def xmlTags(self):
        return self._tags

    @documentFrequencies.getter
    def documentFrequencies(self):
        return self._df

    @termFrequencies.getter
    def termFrequencies(self):
        return self._tf

    @documentsLength.getter
    def documentsLength(self):
        return self._dl

    @porterStemmer.setter
    def porterStemmer(self, val):
        self._stemmer = val

    @stopWords.setter
    def stopWords(self, val):
        self._stopwords = val


    def getXmlPath(self, token: str, docId: str, tag: str) -> list:
        """Returns a list of xml paths of a pair token, doc ID and element xml tag"""
        if ((token,docId) in self._xp.keys()):
            if(tag in self._xp[(token,docId)]):
                return self._xp[(token,docId)][tag]
            raise KeyError("The token,docId, tag pair \"" + token, docId,tag + "\" is unknown")
        raise KeyError("The token,docId pair \"" + token, docId + "\" is unknown")

    def getXmlPathALTER(self, token: str, docId: str) -> list:
        """Returns a list of xml paths of a pair token, doc ID and element xml tag"""
        if ((token, docId) in self._xp.keys()):
            return self._xp[(token, docId)]
        else:
            return []
       # raise KeyError("The token,docId pair \"" + token, docId + "\" is unknown")

    def getDocumentFrequency(self, token: str) -> int:
        """Returns the document frequency of token"""
        if token in self._df.keys():
            return self._df[token]
        raise KeyError("The token \"" + token + "\" is unknown")

    def getTermFrequency(self, token: str, docId: int) -> int:
        """Returns the term frequency of token in document docId"""
        if token in self._tf.keys():
            if docId in self._tf[token]:
                return self._tf[token][docId]
            raise KeyError("The token \"" + token + "\" does not appear in D" + str(docId))
        raise KeyError("The token \"" + token + "\" is unknown")

    def printTermFrequency(self,token: str, docId: int) -> None:
        Nbterm = self.getTermFrequency(token,docId)
        percentage = Nbterm / self.getDocumentFrequency(token) * 100
        print("Term : "+token+" | Nombre occurence dans le document "+str(docId)+" : "+ str(Nbterm)+" | Percentage total : " +str(percentage)+"%")

    def getDocumentLength(self, docID) -> int:
        return self._dl[docID]

    def index(self, *files, progress: bool = True) -> int:
        """Indexes one or multiple files and return number of tokens"""
        tokens = []
        nbTokens = 0
        for file in files:
            tokens = self.__readTokens(file, progress)
            nbTokens += len(tokens)
            self.__fromTokens(tokens, progress)
        return nbTokens

    def printIndex(self) -> None:
        print(self.__str__())

    def extract_terms(self, root,  doc_id, path="/article[1]"):
        """Extract terms from all XML tags and return a list of tuples containing the
        term and the XML path of the tag.
        """
        results = []

        if root.text and root.text.strip():
            tag = root.tag
            terms = root.text.strip()
            terms = terms.replace("'", " ")
            terms = re.sub(r'[^a-zA-Z]', ' ', terms)
            for term in terms.split():
                term = term.lower()

                # implemantion stop words
                if(self._stopwords):
                    if(term in stopwords):
                        continue

                # implementation stemmer
                if(self._stemmer):
                    term = ps.stem(term)

                # default implementation
                results.append((term, path))

                # store xml paths
                if((term,doc_id) in self._xp.keys()):
                    if tag in self._xp[(term,doc_id)]:
                        self._xp[(term,doc_id)][tag].append(path)
                    else:
                        self._xp[(term, doc_id)][tag] = [path]
                else:
                    self._xp[(term,doc_id)] = dict()
                    self._xp[(term,doc_id)][tag] = [path]

        tempDICT = {}
        for child in root:
            if doc_id not in self._tags.keys():
                self._tags[doc_id] = [child.tag]
            else:
                self._tags[doc_id].append(child.tag)
            if child.tag not in tempDICT:
                tempDICT[child.tag] = 1
            else:
                tempDICT[child.tag] += 1
            child_path = f"{path}/{child.tag}[{tempDICT[child.tag]}]"
            results.extend(self.extract_terms(child, doc_id, child_path))
        return results

    def __readTokens(self, file, progress: bool = True) -> list:
        """Reads the tokens from file"""
        file = open(file, mode='r', encoding='utf8')
        file_content = file.read()
        file_content = file_content.replace("&nbsp", "")
        file_content = file_content.replace("&mdash", "")
        file_content = file_content.replace("&lsaquo", "")
        file_content = file_content.replace("&copy", "")
        file_content = file_content.replace("&euro", "")
        file_content = file_content.replace("&ndash", "")
        file_content = file_content.replace("&middot", "")
        file_content = file_content.replace("&rsaquo", "")
        file.close()

        id = path.basename(file.name).split('.')[0]


        parsed_file = ET.fromstring(file_content)
        words = self.extract_terms(parsed_file, id)

        tokens = []

        self._dl[id] = len(words)

        for token in words:
            token = token[0]
            if(self._stopwords):
                if token in stopwords:
                    continue
            tokens += [(token, id)]

        tokens.sort(key = itemgetter(WORD, DOCID))
        return tokens

    def __fromTokens(self, tokens, progress: bool = True) -> None:
        """Takes the tokens and put them in the posting lists"""
        for token in tqdm(tokens, desc="Indexing tokens", colour='green', disable=not progress, unit='token'):
            word = token[WORD]
            docId = token[DOCID]

            if word not in self._pl.keys():
                self._pl[word] = [docId]
                self._tf[word] = dict()
                self._tf[word][docId] = 1
            else:
                if docId not in self._pl[word]:
                    self._pl[word] += [docId]
                    self._tf[word][docId] = 1
                else:
                   self._tf[word][docId] += 1

        self.__calculateDocumentFrequency()

    def __calculateDocumentFrequency(self) -> None:
        for token, docIds in self._pl.items():
            self._df[token] = len(docIds)


    def saveIndexAs(self, filename: str) -> bool:
        """Saves the index as a binary file"""
        try:
            file = open(filename, "wb")
            pickle.dump(self, file)

            file.close()
            return True
        except:
            print(f"[ERROR] File {filename} could not be saved", file=stderr)
            return False

    def loadIndexAs(self,filename: str):
        """Returns an indexer pre-indexed loaded from a binary file"""
        file = open(filename,"rb")

        temp = pickle.load(file)
        file.close()

        return temp


    def __str__(self) -> str:
        desc = ""
        for token, docIds in self._pl.items():
            desc += str(self._df[token]) + "=df(" + token + ")\n"
            for docId in docIds:
                desc += "\t" + str(self._tf[token][docId]) + " D" + str(docId) + "\n"
        return desc

    @property
    def dl(self):
        return self._dl
