import xml.etree.ElementTree as ET
from indexer import Indexer
from pathlib import Path
from collections import Counter
import re
#le repertoire des data du tp5
directory = 'data_tp5'
files = Path(directory).glob('*')


def extract_terms(root, path=""):
    """Extract terms from all XML tags and return a list of tuples containing the
    term and the XML path of the tag.
    """
    results = []

    if root.text and root.text.strip():
        for term in root.text.strip().split():
            results.append((term, path))
    tempDICT = {}
    for child in root:
        if child.tag not in tempDICT:
            tempDICT[child.tag] = 1
        else:
            tempDICT[child.tag] += 1
        child_path = f"{path}/{child.tag}[{tempDICT[child.tag]}]"

        results.extend(extract_terms(child, child_path))
    return results

for file in files:
    file = open(file, mode='r', encoding='utf8')
    file_content = file.read()
    file_content = file_content.replace("&nbsp", "")
    file_content = file_content.replace("&mdash", "")
    file_content = file_content.replace("&lsaquo", "")
    file_content = file_content.replace("&copy", "")
    file_content = file_content.replace("&euro", "")
    file_content = file_content.replace("&ndash", "")
    file_content = file_content.replace("&middot", "")
    file_content = file_content.replace("&rsaquo", "")
    file.close()

    parsed_file = ET.fromstring(file_content)
    results = extract_terms(parsed_file)
    print(results)
    break
